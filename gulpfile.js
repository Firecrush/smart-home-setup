/**
 * Created by Lukas on 13.10.2017.
 */
const gulp = require('gulp');
const request = require('superagent');
const shell = require('gulp-shell');
const fs = require('fs');
const clean = require('gulp-clean');
const install = require('gulp-install');
const readline = require('readline');
let projectName;

gulp.task('getProjectName', (cb) => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the project name ', (answer) => {
    projectName = answer;
    rl.close();
    process.stdin.destroy();
    cb();
  });
});

gulp.task('createGitRepo', ['createProject'], () => {
  return request.post('https://gitlab.com/api/v4/projects?private_token=Sa8RDWsN_ZqLQLrJs1a7').send({name: projectName})
    .then((result) => {
      console.log(JSON.stringify(result, null, 2))
    })
    .catch((err) => {
      console.error(JSON.stringify(err, null, 2))
    })
});

gulp.task('cleanGit', ['createGitRepo'], () => {
  return gulp.src(`testProject/.git`, {read: false})
    .pipe(clean())
});

gulp.task('initRepo', ['move'],() => {
  return gulp.src(`../${projectName}/**.js`).pipe(
    shell([
      'git init',
      `git remote add origin git@gitlab.com:Firecrush/${projectName}.git`,

    ], {cwd: `../${projectName}`})
  );
});

gulp.task('npmInstall', ['initRepo'], () => {
  return gulp.src(`../${projectName}/package.json`).pipe(install());
});

gulp.task('cleanSetup', ['npmInstall'], () => {
  return gulp.src(`${projectName}`).pipe(clean());
});

gulp.task('move', ['setPackageJsonRepo'], () => {
  return gulp.src(`./${projectName}/**`, {base: '.'}).pipe(gulp.dest(`..`))
});

gulp.task('setPackageJsonName', ['cleanGit'], () => {
  const packagejson = require(`./${projectName}/package.json`);
  packagejson.name = projectName;
  fs.writeFileSync(`${projectName}/package.json`, JSON.stringify(packagejson, null, 2));
});

gulp.task('setPackageJsonRepo', ['setPackageJsonName'], () => {
  const packagejson = require(`./${projectName}/package.json`);
  packagejson.repository = {
    type: 'git',
    url: `git@gitlab.com:Firecrush/${projectName}`
  };
  fs.writeFileSync(`${projectName}/package.json`, JSON.stringify(packagejson, null, 2));
});

gulp.task('cleanProject', ['getProjectName'], () => {
  return gulp.src(`${projectName}`).pipe(clean())
});

gulp.task('createProject', ['cleanProject'], () => {
  return gulp.src('gulpfile.js').pipe(
    shell([
      'git clone https://gitlab.com/Firecrush/smart-home-boiler-plate.git',
      `rename smart-home-boiler-plate ${projectName}`,
      `cd ${projectName}`
    ]));
});

gulp.task('setupProject', ['cleanSetup'], () => {

});
